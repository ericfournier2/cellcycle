pushd output/pipeline

# Generate the chrom size file.
cut -f 1-2 /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai > chrom.sizes

module load mugqic/ucsc/v326
for sampleDir in peak_call/*
do
    sampleName=`basename $sampleDir`
    peakFile=$sampleDir/$sampleName"_peaks.narrowPeak"
    
    cut -f 1-3 $peakFile > $sampleDir/$sampleName.bed
    bedToBigBed $sampleDir/$sampleName.bed chrom.sizes $sampleDir/$sampleName.bb
done

popd