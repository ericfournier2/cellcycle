cd /gs/scratch/efournier/CellCycle/output/pipeline

for sample in alignment/*
do
    sampleName=`basename $sample`    

    cat > $sampleName.sh <<EOF
#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l walltime=8:00:00
#PBS -A fhh-112-aa
#PBS -o $sampleName.sh.stdout
#PBS -e $sampleName.sh.stderr
#PBS -V
#PBS -N $sampleName.sh

module load mugqic/samtools/1.3 mugqic/bedtools/2.22.1 mugqic/ucsc/20140212 mugqic/python/2.7.8

cd /gs/scratch/efournier/CellCycle/output/pipeline

mkdir -p tracks/$sampleName tracks/bigWig

/home/efournier/.local/bin/bamCoverage --normalizeUsingRPKM --extendReads 200 --binSize 10 \
                                       --bam alignment/$sampleName/$sampleName.sorted.dup.bam \
                                       --outFileFormat bedgraph  --outFileName tracks/$sampleName/extended.bedgraph

bedGraphToBigWig tracks/$sampleName/extended.bedgraph /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai  tracks/bigWig/$sampleName.ucsc.extended.bw
EOF

    qsub $sampleName.sh
done
